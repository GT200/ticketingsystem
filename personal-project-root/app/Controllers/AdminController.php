<?php

namespace App\Controllers;

use App\Models\Data;

class AdminController extends BaseController
{
	public function setCovidMode(){
		//initiate connection to db
		$model = model(Data::class); 

		$tb_name = $this->request->getVar("tb_name");
		$league = $this->request->getVar("league");
		$idMatch = $this->request->getVar("idMatch");
		//get date data from web scraping
		$model->SetCovidConfig($tb_name, "reservation","COVID", "numid");
		$model->updateToDable($league, "covid",1, "id", $idMatch);
		//send date data to calender.js 
		return json_encode($tb_name);
	}

	public function unSetCovidMode(){
		//initiate connection to db
		$model = model(Data::class); 

		$tb_name = $this->request->getVar("tb_name");
		$league = $this->request->getVar("league");
		$idMatch = $this->request->getVar("idMatch");
		//get date data from web scraping
		$model->SetCovidConfig($tb_name, "reservation","unreserved", "numid");
		$model->updateToDable($league, "covid",0, "id", $idMatch);
		//send date data to calender.js 
		return json_encode($tb_name);
	}

	public function adminDeleteMatch(){
		//initiate connection to db
		$model = model(Data::class); 

		$idMatch = $this->request->getVar("idMatch");
		$tb_name = $this->request->getVar("tb_name");
		echo $idMatch;

		//get date data from web scraping
		 $model->deleteMatch($tb_name, "id", $idMatch);
		//send date data to calender.js 
		return json_encode($tb_name);
	}
}
?>