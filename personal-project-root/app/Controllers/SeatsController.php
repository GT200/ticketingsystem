<?php

namespace App\Controllers;

use Exception;

use App\Models\Data;

use App\Controllers\TicketController;

// helper('form');

class SeatsController extends BaseController
{
	public function seatsDataFromDBtoView(){
		$model = model(Data::class);
		$tb_name = $this->request->getVar("tableId");
		$seats = $model->selectAllData($tb_name );
		$seatsForJs=[];
		foreach($seats as $row){
			$seatsForJs[] = $row;
		};
		return json_encode($seatsForJs);
	}

	public function jsChoosenSeatToDB(){
		$myTicketController = new TicketController();
		$model = model(Data::class);
		$tb_name = $this->request->getVar("tableId");
		$ReserverName = strtoupper($this->request->getVar("ReserverName"));
		// echo "tb_name";
		$choosenSeatsId = json_decode($this->request->getVar("choosenSeatsId"), true);
		$model = model(Data::class);
		$num_sit = [];
		$ticket = '';
		$countTicket = count($choosenSeatsId);
		$ticket_num = 0;
		$match_info = $model->selectWithCondition("frenchMatch", "seatstableid", $tb_name)[0];
		foreach($choosenSeatsId as $key => $val){
			$ticket_num +=1;
			$num_sit[] = "$key";
			$ticketId = strtoupper(substr($ReserverName,0,2)."$key".((string)  random_int(1000, 10000))).":FR:".explode("_",$tb_name)[count(explode("_",$tb_name)) - 1];
			$model->updateToDable($tb_name,"reservation", "reserved", "numiD", "$key");
			$model->updateToDable($tb_name,"ticketId", $ticketId, "numiD", "$key");
			$model->updateToDable($tb_name,"ticketReserver", $ReserverName, "numiD", "$key");
			$model->updateToDable($tb_name,"ticketState", "paid", "numiD", "$key");
			// foreach ($match_info_T as $row) {
            //     $match_info = $row;
            // }
			$OneTicket = $myTicketController->ticketHtmlGenerator($ticketId, $ReserverName, $countTicket, $ticket_num,$match_info);
			$ticket = $ticket.$OneTicket;
		};
		
		return json_encode($ticket);
	}

}
