<?php

namespace App\Controllers;

use Exception;

use App\Models\Data;

class matchDataToSeatsDataController extends BaseController
{
	/*
	this method is use only once, when the seats table for a match is not yet populated
	*/
	public function createSeatsDataFromSelectedMatchData()
    {
		$arraySeatCompleteData = json_decode(json_encode($this->request->getVar()), true);
		$dataMatch = [$arraySeatCompleteData["team1"], $arraySeatCompleteData["team2"], $arraySeatCompleteData["date"], $arraySeatCompleteData["time"], $arraySeatCompleteData["place"]];

		$model = model(Data::class);
		$match = $model->getIdofmatchOrCheckmatch($dataMatch);
		$tb_name = "";
		foreach ($match as $row){
			$tb_name = $row["seatstableid"];
		};
		$rows =  $model->checkSeatsExistance($tb_name);

		$countSeats = 0;
		foreach($rows as $row) {
			$countSeats = $row["countseats"];
		}


		if ($countSeats ==0){
			foreach($arraySeatCompleteData["seat"] as $key => $val) {
				$ret = "$key:$val";
				$price = $arraySeatCompleteData["price"][$key];
				$model->addDataToDable($tb_name, "id, numid, reservation, price", "'$key', '$val', 'unreserved', '$price'");
			  }
		}
		return json_encode(array($tb_name));
	}
}
?>
