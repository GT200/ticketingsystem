<?php

namespace App\Controllers;

use Exception;

use App\Models\Data;

class TicketController extends BaseController
{


	public function ticketHtmlGenerator($ticketId, $ReserverName, $countTicket, $ticket_num,$match_info){
        $code = 'ID:'.$ticketId.'Name:'.$ReserverName;
		$OneTicket = '<div class="courses-container">
		<div class="course">
			<div class="course-preview">
				<h6>TICKET</h6>
                 <img style="width:100%; height:100%; margin:auto;" src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl='.$code.'&choe=UTF-8">
			</div>
			<div class="course-info">
				<h6>ID: '.$ticketId.'</h6>
				<p>Owner: '.$ReserverName.'</p>
				<p>Nomber of ticket: '.$countTicket.'</p>
				<p>Ticket: '.$ticket_num.'/'.$countTicket.'</p>
				<p>Match: '.$match_info["team1"].'VS'.$match_info["team2"].'</p>
				<p>Date: '.$match_info["thedate"].'</p>
				<p>Time: '.$match_info["thetime"].'</p>
				<p>Location: '.$match_info["place"].'</p>
			</div>
		</div>
	</div>';
		return $OneTicket;
	}


	public function cancelTicket(){
		$ticketId = $this->request->getVar("ticketID");
		$model = model(Data::class);
		try
		{
			$match_id = explode(":",$ticketId)[count(explode(":",$ticketId)) - 1];
			$match_tag = explode(":",$ticketId)[count(explode(":",$ticketId)) - 2];

			if($match_tag=="FR")
			{
				$tb_name = "fr_match_seat_table_id_".$match_id;
				$arrayTicketID = $model->selectWithCondition($tb_name, "ticketId",  "$ticketId");

				if (count($arrayTicketID) == 0)
				{
						return json_encode(array("Sorry men, your ticket dont exists the the database"));
					}
				else if ($arrayTicketID[0]["ticketstate"]=="canceled")
				{
					return json_encode(array("your ticket has already been canceled"));
				}
				else
				{
					$model->updateToDable($tb_name,"reservation", "unreserved", "ticketId", "$ticketId");
					$model->updateToDable($tb_name,"ticketState", "canceled", "ticketId", "$ticketId");
					return json_encode(array("ticket canceled successfully"));
				}	
			}
		}
		catch (Exception $ex)
		{
			// $ex->getMessage()
			return json_encode(array("invalid ticket format"));
		}
	}
}
