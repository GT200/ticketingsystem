<?php

namespace App\Controllers;

use App\Models\Data;

class CalenderController extends BaseController
{
	public function sendDateToCalender(){
		//initiate connection to db
		$model = model(Data::class); 
		//set the url for websraping
		$url = "https://nm1.ffbb.com/calendrier";
		//get date data from web scraping
		$dates = $model->webScrabFrenchDate($url)[1];
		//send date data to calender.js 
		return json_encode($dates);
	}
}
?>