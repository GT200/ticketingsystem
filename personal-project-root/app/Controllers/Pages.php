<?php

namespace App\Controllers;

use App\Models\Data;

class Pages extends BaseController
{
    
    public function showFrenchLeagueMatch($page = 'welcome_message', $title = "ENG", $hrefcalender='firstVisit'){
        /*
        cette fonction récuprère des données de match en temps réel sur le site https://nm1.ffbb.com/calendrier par web srappimg et le enregistre dans la base de donnée 
        si elle n'existe pas encore
        */
        if (! is_file(APPPATH . 'Views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
        

        $data['title'] = ucfirst($title); // Capitalize the first letter

        $model = model(Data::class);

        if ($hrefcalender=='firstVisit')
        {
            $hrefcalender = $model->webScrabFrenchDate("https://nm1.ffbb.com/calendrier")[0][0];
        }
        else{
            $hrefcalender = str_replace("(", "/", $hrefcalender);
        }

        

        $url = 'https://nm1.ffbb.com/'.$hrefcalender;
        // echo($url);

        $date = $model->webScrab($url);

        $FrData = $model->getDataFR($date)[0];
        $NumFrData = $model->getDataFR($date)[1];

        $data['frData'] = $FrData;

        $data['frData'] = $FrData;

        $data['NumFrData'] = $NumFrData;



        return view('templates/header', $data)
            . view('pages/' . $page, $data)
            . view('templates/footer');
    }

    public function diplaySeats($title, $matchtable, $date, $time, $place)
    {
        /*
        cette contion affiche les places d'un match données
        */
        $data["title"] =$title;
        $data['matchtable'] = $matchtable;
        $data["date"] =$date;
        $data["time"] =$time;
        $data["place"] =$place;

        return view('templates/header', $data).
                view('pages/seats',  $data).
                view('templates/footer');
    }

    
    public function adminPage($hrefcalender='firstVisit')
    {
        /*
        affiche la page de l'admin
        */

        $model = model(Data::class);

        if ($hrefcalender=='firstVisit')
        {
            $hrefcalender = $model->webScrabFrenchDate("https://nm1.ffbb.com/calendrier")[0][0];
        }
        else
        {
            $hrefcalender = str_replace("(", "/", $hrefcalender);
        }

        

        $url = 'https://nm1.ffbb.com/'.$hrefcalender;
        // echo($url);

        $date = $model->webScrab($url);

        $FrData = $model->getDataFR($date)[0];
        $NumFrData = $model->getDataFR($date)[1];

        $data['frData'] = $FrData;

        $data['frData'] = $FrData;

        $data['NumFrData'] = $NumFrData;

        return view('templates/headerAdmin', $data).
                view('pages/admin2',  $data);
    }

}
