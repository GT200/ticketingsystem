<!doctype html>
<html>
<head>
    <title>Magic ticketing system</title>
    <!-- leaflet -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.2/dist/leaflet.css" integrity="sha256-sA+zWATbFveLLNqWO2gtiw3HL/lh1giY/Inf1BJ0z14=" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.9.2/dist/leaflet.js" integrity="sha256-o9N1jGDZrf5tS+Ft4gbIK7mYMipq9lqpVJ91xHSyKhg=" crossorigin=""></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <!-- bootstrap-->    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>  
    <!--anychart -->    
    <!-- <script src="https://cdn.anychart.com/releases/8.11.0/js/anychart-core.min.js"></script>
    <script src="https://cdn.anychart.com/releases/8.11.0/js/anychart-base.min.js"></script>
    <script src="https://cdn.anychart.com/releases/8.11.0/js/anychart-bundle.min.js"></script> -->
    <!--anychart -->  
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script> -->
    <!--OwlCarousel2 -->  
    <!-- https://github.com/OwlCarousel2/OwlCarousel2 -->
    <link rel="stylesheet" href="<?php echo base_url('assests/OwlCarousel2-2.3.4/dist/assets//owl.carousel.min.css'); ?>"/>
    <script src="<?php echo base_url('assests/OwlCarousel2-2.3.4/dist/owl.carousel.min.js'); ?>"></script>
    <!--personal css -->  
    <link href="<?php echo base_url('css/header.css'); ?>" rel="stylesheet" type="text/css"> 
    <link href="<?php echo base_url('css/carousel.css'); ?>" rel="stylesheet" type="text/css"> 
    <link href="<?php echo base_url('css/style.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('css/payment.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('css/seat.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('css/homePage.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('css/admin.css'); ?>" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="<?php echo base_url('assests/PrintJS/print.min.css'); ?>" type="text/css">
    <script src="<?php echo base_url('assests/PrintJS/print.min.js'); ?>"></script>

</head>
<body>

  <header>
    <div class="overlay">
      <h1><?= esc($title) ?></h1>
      <div id ="dateHeader"></div>
      <button id="homePage" onclick="window.location.href='http://localhost:8080/'">HOME PAGE</button>
      <!-- Button trigger modal -->
      <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalHeader" id="cancelModelBtn">CANCEL TICKET</button>

      <!-- Modal -->
      <div class="modal fade" id="exampleModalHeader" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">

            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Cancel a reservation</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
              <form>
                <div class="mb-3">

                  <label for="Ticket ID" class="col-form-label" required>Ticket ID:</label>
                  <input type="text" class="form-control" id="cancledTicketId">

                  <label for="Reserver name" class="col-form-label" required>Reserver name</label>
                  <input type="text" class="form-control" id="cancelTicketReserverName" value="KUROKO">

                  <label for="Reserver email" class="col-form-label" required>Reserver email</label>
                  <input type="email" class="form-control" id="cancelReserverEmail" value="KUROKO@serin.anime">

                </div>
                <div class="mb-3">

                  <label for="message-text" class="col-form-label">VISA ACOUNT NAME</label>
                  <input type="text" class="form-control" id="cancledVisaAccountName"  value="KUROKO">
                  <label for="message-text" class="col-form-label">IBAN</label>
                  <input type="text" id="cancelIban" name="iban" placeholder="IBAN" value="KU7630004015870002601171220">
                </div>
              </form>
            </div>

            <div class="modal-footer">
              <button id="cancelReservation" class="btn btn-secondary"  data-bs-toggle="tooltip" data-bs-placement="top" title="Tooltip on top">CONFIRM TICKET CANCELLATION</button>
            </div>

          </div>
        </div>
      </div>
    </div>
  </header>
  <script src="<?php echo base_url('js/cancelTicket.js'); ?>" ></script>

