<div id="svgContainer" style="display:none">

<svg xmlns="http://www.w3.org/2000/svg" width="100px" height="100px">
    <!-- <circle r="50" cx="50" cy="50" fill="green"/> -->
  </svg>
</div>

<div id="seatMapContainer">
<div id="tooltip" >tooltip </div>
  <div id="map" >

  </div>

  <div style="display:flex; flex-direction:column; justify-content:space-between;" >

    <div class="card seatsInfoCard" style="width: 18rem;height: 200px; overflow:auto; justify-content:space-evenly;">
    <img style="width:50%; height:50%; margin:auto;" class="card-img-top" src="<?php echo base_url('img/dribleGIF.gif'); ?>" alt="Card image cap">
    <div class="card-body">
      <div class="card-title" style="text-align:center;"><strong>Match information</strong> </div>
      <div class="card-text" id="matchInformationForTickets"> <?php echo "<strong>date:</strong> $date <br> <strong>time:</strong>$time <br> <strong>location:</strong>$place";?> </div>
    </div>

  </div>

  
  <div class="card seatsInfoCard"  style="width: 18rem;height: 300px" >
  <div class="card-header" style="text-align:center;">
    <strong>choose seat</strong>
  </div>
  <div class="card-body" style="overflow:auto;">
      <ul class="list-group list-group-flush"  id = "choosenSeats">
          <!-- <?php for($i =0;$i<1;$i++):?>
              <li class="list-group-item">choose seats  <?php echo $i?></li>
              <?php endfor; ?>  -->
          
        </ul> 



  </div>
  <div class="card-footer" id="sumPrice">
    <strong>Total price:</strong>
  </div>
  </div>

  <div class="card text-center seatsInfoCard" >
  <div class="card-body">

<!-- Button trigger modal -->
<a type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Confirm choosen seats
          </a>
  </div>
  </div>




  </div>

  </div>
    


<div id="tableID" style="display:none" ><?php echo $matchtable;?></div>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
        <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
      </div>
      <div class="modal-body" id="transToTickt" >



      <div class="paymentContainer p-0" id = "paymentContainer">
        <div class="card px-4">
            <p class="h8 py-3">Payment Details</p>
            <div class="row gx-3">
                <div class="col-12">
                    <div class="d-flex flex-column">
                        <p class="text mb-1">Person Name</p>
                        <input id="inputReserverName" class="form-control mb-3" type="text" placeholder="Name" value="Kuroko">
                    </div>
                </div>
                <div class="col-12">
                    <div class="d-flex flex-column">
                        <p class="text mb-1">Card Number</p>
                        <input class="form-control mb-3" type="text" placeholder="1234 5678 435678" value="1234 5678 435678">
                    </div>
                </div>
                <div class="col-6">
                    <div class="d-flex flex-column">
                        <p class="text mb-1">Expiry</p>
                        <input class="form-control mb-3" type="text" placeholder="MM/YYYY">
                    </div>
                </div>
                <div class="col-6">
                    <div class="d-flex flex-column">
                        <p class="text mb-1">CVV/CVC</p>
                        <input class="form-control mb-3 pt-2 " type="password" placeholder="***">
                    </div>
                </div>
                <div class="col-12">
                    <div class="btn btn-primary mb-3">
                        <span class="ps-3" id="payTotalPrice">Pay</span>
                        <span class="fas fa-arrow-right"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
        



      </div>
      <div class="modal-footer justify-content-center flex-column flex-md-row" id="paymentCardFooter">
      <button type="button" class="btn btn-secondary mr-auto" data-bs-dismiss="modal">Abandon pyment</button>
        <!-- <a href="" class="btn btn-primary mr-auto" id="submitChooseSeat">Confirm payment</a>  -->
      <button type="button" class="btn btn-primary mr-auto" id="submitChooseSeat">Confirm payment</button>

        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>


 <button id="printBtn" type="button" class="btn btn-primary mr-auto" style="display:none;" onclick="printJS({
		printable: 'transToTickt',
		type: 'html',
		properties: ['name', 'email', 'phone'],
		style: '.course{background-color: #33fb9b; border-radius: 10px; box-shadow: 0 10px 10px rgba(0, 0, 0, 0.2); display: flex; max-width: 100%; margin: 1px; overflow: hidden; width: 100%; font-size: 15px; } .course h6 { font-size: 15px; margin: 0; letter-spacing: 1px; text-transform: uppercase; background-color: #67ff00ba; color: black; } .course p {margin: 0;} '
	  })">
	Print ticket
</button> 

<script src="<?php echo base_url('js/seats.js'); ?>" ></script>

<script>
  
const seatsObj = new Seats("tableID");

console.log(seatsObj.seatsDBTableName);

seatsObj.addSeatMapSvgToMap("<?php echo base_url('/img/seats3.svg'); ?>");

seatsObj.fetchDataFromDataBase();

seatsObj.addEventOnSeatClick();

</script>

