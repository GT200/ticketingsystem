
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
  <label class="form-check-label" for="inlineRadio1">French</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
  <label class="form-check-label" for="inlineRadio2">NBA</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3" disabled>
  <label class="form-check-label" for="inlineRadio3">3 (disabled)</label>
</div>



<div class="wrapper" id = "carouselContainer">
    <div class="carousel owl-carousel" id = "carousel">
      <?php for ($i = 0; $i <$NumFrData; $i++): ?>
        <!-- <div id="tableID" style="display:none" ><?php echo $frData[$i]["seatstableid"];?></div> -->
        <div class="card card-<?php echo $i; ?>">
          <h5 class="team1"><?php echo $frData[$i]["team1"];?></h5>
          <p>VS</p>
          <h5 class="team2"><?php echo $frData[$i]["team2"];?></h5>

          <div class="locationTime">
            <div class="MiniLocationTime">Stade: 
              <div id= "place"><?php echo $frData[$i]["place"];?> </div> 
            </div> 
            <div class="MiniLocationTime">Heure debut:
              <div id = "time"> <?php echo $frData[$i]['thetime']?></div>
            </div>
          </div>

          
        <button type="button" class="reservation_button" id="button-<?php echo $i; ?>">Reserver</button>
        </div>
      <?php endfor; ?>


    </div>
</div>

  <div class="calendar"></div>

<script src="<?php echo base_url('js/dynamcUI.js'); ?>" ></script>

<script src="<?php echo base_url('js/calender.js'); ?>" ></script>

<script src="<?php echo base_url('js/leagueToSeatMap.js'); ?>" ></script>

<script>
  document.getElementById("dateHeader").innerHTML= '<span>Match on :</span> <div id = "date"> <?php echo $frData[0]["thedate"];?></div>'
</script>


