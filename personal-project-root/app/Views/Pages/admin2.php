<div id="adminChooseLeagueRadioButton">
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" checked >
        <label class="form-check-label" for="inlineRadio1">French</label>
    </div>
    <!-- <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
        <label class="form-check-label" for="inlineRadio2">NBA</label>
    </div>  -->
</div>

<div class="wrapper" id = "maintMatchContainter">

  <div class="card border-success mb-3" id="adminMathCard" style="max-width: 70%; max-height: 500px;">
    <div class="card-header bg-transparent border-success" id="dateHeader">Header</div>
    <div class="card-body text-success">
      <!--<h5 class="card-title">Success card title</h5>  -->

      <?php for ($i = 0; $i <$NumFrData; $i++): ?>
 

        <div class="card-text matchList"> ID:  <?php echo $frData[$i]["id"];?> <?php echo $frData[$i]["team1"];?> VS <?php echo $frData[$i]["team2"];?> Time: <?php echo $frData[$i]["thetime"];?>  Location: <?php echo $frData[$i]["place"];?></div>

       
      <?php endfor; ?>

    </div>
    
  </div>

  <div class="card border-success mb-3" id="adminSetCovidCard" >
    <div class="card-header bg-transparent border-success" id="dateHeader">Covid configuration</div>
    <div class="card-body text-success">
      <!--<h5 class="card-title">Success card title</h5>  -->

      <?php for ($i = 0; $i <$NumFrData; $i++): ?>
        <?php if ($frData[$i]["covid"] == 1): ?>
     
        <div class="form-check form-switch matchList">
          <input class="form-check-input covidSwitch" type="checkbox" id="matchID-<?php echo $frData[$i]["id"];?>" value="{'team1':'<?php echo $frData[$i]["team1"];?>', 'team2':'<?php echo $frData[$i]["team2"];?>', 'time':'<?php echo $frData[$i]["thetime"];?>', 'place': '<?php echo $frData[$i]["place"];?>'}" checked>
          <label class="form-check-label" for="matchID-<?php echo $frData[$i]["id"];?>"><?php echo $frData[$i]["id"];?> <?php echo $frData[$i]["covid"];?> COVID</label>
        </div>

        <?php else: ?>
          <div class="form-check form-switch matchList">
          <input class="form-check-input covidSwitch" type="checkbox" id="matchID-<?php echo $frData[$i]["id"];?>"  value="{'team1':'<?php echo $frData[$i]["team1"];?>', 'team2':'<?php echo $frData[$i]["team2"];?>', 'time':'<?php echo $frData[$i]["thetime"];?>', 'place': '<?php echo $frData[$i]["place"];?>'}">
          <label class="form-check-label" for="matchID-<?php echo $frData[$i]["id"];?>"><?php echo $frData[$i]["id"];?> COVID</label>
        </div>

        <?php endif; ?>

       
      <?php endfor; ?>

    </div>

  </div>

  <!--<div class="card border-success mb-3" id="deleteMatch" >
    <div class="card-header bg-transparent border-success" id="dateHeader">Covid configuration</div>
    <div class="card-body text-success">
      <h5 class="card-title">Success card title</h5>  

      ?php for ($i = 0; $i <$NumFrData; $i++): ?>


          <button class="deleteMatch matchList deleteBtn" id="deleteMatchID-?php echo $frData[$i]["id"];?>" value="?php echo $frData[$i]["id"];?>" style="display:block;">Delete ?php echo $frData[$i]["id"];?></button>
       
      ?php endfor; ?>

    </div>

  </div>
  -->



</div>

<div class="calendar"></div>

<script>
  document.getElementById("dateHeader").innerHTML= '<span>Match on :<span id = "date"><?php echo $frData[0]["thedate"];?></span>  </span> '
</script>

<script src="<?php echo base_url('js/dynamcUI.js'); ?>" ></script>

<script src="<?php echo base_url('js/adminSeatsCovidMod.js'); ?>" ></script>



<script src="<?php echo base_url('js/calenderAdmin.js'); ?>" ></script>

<script src="<?php echo base_url('js/admin.js'); ?>" ></script>

<!--<script src="php echo base_url('js/adminDeleteMatch.js'); " ></script>-->


