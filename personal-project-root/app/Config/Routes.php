<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('/seats/(:any)/(:any)/(:any)/(:any)/(:any)', 'Pages::diplaySeats/$1/$2/$3/$4/$5');
// $routes->get('createSeats', 'matchDataToSeatsDataController::createSeatsDataFromSelectedMatchData');
$routes->get('/league/(:any)/(:any)/(:any)', 'Pages::showFrenchLeagueMatch/$1/$2/$3');
$routes->get('/admin/(:any)', 'Pages::adminPage/$1');


$routes->get('/testWebScrap', 'SeatsController::testWebScrap');

$routes->post('/calender', 'CalenderController::sendDateToCalender');
$routes->post('/sendNewMatchToCalender', 'SeatsController::sendNewMatchToCalender');
$routes->post('/seatstoDB', 'matchDataToSeatsDataController::createSeatsDataFromSelectedMatchData');
$routes->post('/seatsDBtoView', 'SeatsController::seatsDataFromDBtoView');
$routes->post('/jsChoosenSeatToDB', 'SeatsController::jsChoosenSeatToDB');
$routes->post('/cancelTicket', 'TicketController::cancelTicket');

$routes->post('/setCovid', 'AdminController::setCovidMode');
$routes->post('/unSetCovid', 'AdminController::unSetCovidMode');
$routes->post('/adminDelete', 'AdminController::adminDeleteMatch');


 

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
