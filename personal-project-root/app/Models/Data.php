<?php

namespace App\Models;

use CodeIgniter\Model;

use Exception;

use DateTime;

class Data extends Model
{
    // private const host='localhost';
    // private const user='postgres';
    // private const password='postgres';


    public function addDataToDable($tablename,$attributes,   $values){
        $query2 = "INSERT INTO $tablename($attributes) VALUES ($values);";
        // echo  $query2;
        $result = $this->db->query($query2);
    }

    public function updateToDable($tablename, $colName,$value, $colNameCondition, $valueContition){
        $query2 = "UPDATE $tablename SET $colName='$value' WHERE $colNameCondition='$valueContition';";
        // echo  $query2;
        $result = $this->db->query($query2);
    }

    public function checkSeatsExistance($tb_name){
        $query = "SELECT COUNT(*) as countseats FROM $tb_name;";
        // echo $query;
        $query = $this->db->query($query);
        
        $row = $query->getResultArray();
        return $row ;
    }

    public function SetCovidConfig($tablename, $colName,$value, $colNameCondition){
    
        $query2 = "UPDATE $tablename SET $colName='$value' WHERE mod(CAST($colNameCondition AS UNSIGNED), 2)=0;";
        // echo  $query2;
        $this->db->query($query2);


    }

    public function deleteMatch($tablename, $colName,$valueCondition){


        try
        {
                    // DELETE FROM daily_sales
        // WHERE store_state = 'DL';
        $query = "DELETE FROM $tablename WHERE $colName='$valueCondition'";
        // echo  $query2;
        $this->db->query($query);
            $query2 = "DROP TABLE fr_match_seat_table_id_".$valueCondition;
        // echo  $query2;
        $this->db->query($query2);

        }
        catch(Exception)
        {
                    // DELETE FROM daily_sales
        // WHERE store_state = 'DL';
        $query = "DELETE FROM $tablename WHERE $colName='$valueCondition'";
        // echo  $query2;
        $this->db->query($query);

        }

        

        

    }

    public function getDataFR($date){
        $query = "SELECT * FROM frenchmatch WHERE thedate = '$date';";
        $query = $this->db->query($query);

        $queryStringResult = $query->getResultArray();

        $queryStringResultNum = $query->getNumRows();

        
        if (isset($queryStringResult)) {
            // foreach ($queryStringResult as $res) {
            //     echo $res->team1;
            // }
                return array($queryStringResult, $queryStringResultNum);
        }
        else{
            return "a problem accurt to get data";
        }
    }

    public function selectWithCondition($tablename, $colNameCondition, $valueContition){
        $query = "SELECT * FROM $tablename WHERE $colNameCondition='$valueContition';";
        $query = $this->db->query($query);

        $queryStringResult = $query->getResultArray();

        return $queryStringResult;

    }


    

    public function selectAllData($tb_name){
        $query = "SELECT * FROM $tb_name;";
        // echo $query;
        $query = $this->db->query($query);
        
        $row = $query->getResultArray();
        return $row ;
    }



    public function getIdofmatchOrCheckmatch($arraymatch){
        $query = "SELECT * FROM frenchmatch WHERE (team1='$arraymatch[0]' AND team2='$arraymatch[1]' AND thedate='$arraymatch[2]' AND thetime = '$arraymatch[3]' AND place = '$arraymatch[4]');";
        // echo $query;
        $query = $this->db->query($query);
        
        $row = $query->getResultArray();
        return $row ;
    }



    public function webScrabFrenchDate($url){
        $html = file_get_contents($url);
        $HrefCalender = [];
        $datesMatch = [];
		while(!Empty(stripos($html, 'class="scroll-entry scroll-entry__schedule "'))){
            $start = stripos($html, 'class="schedule__link" href=');
            $end = stripos($html, '" title', $offset = $start + 5);
            $length = $end - $start;
            $htmlSectionHrefCalender = substr($html, $start, $length);
            $htmlSectionHrefCalender = str_replace('class="schedule__link" href="/', "",$htmlSectionHrefCalender);
            $html =substr($html,  $end);

            $date = explode("/", $htmlSectionHrefCalender)[1];

            $currentDate = new DateTime();
            if (strtotime($date)>strtotime($currentDate->format('Y-m-d'))){
                $HrefCalender[]=$htmlSectionHrefCalender;
                $datesMatch[]=$date;
            }
            // echo $dateMatch>strtotime($currentDate->format('Y-m-d')) ;
        }

        return (array($HrefCalender, $datesMatch));
    }


    public function webScrab($url){

        $query = 'CREATE TABLE IF NOT EXISTS frenchmatch (
            id serial PRIMARY KEY,
            team1 character varying(100) NOT NULL,
            team2 character varying(100) NOT NULL,
            thedate character varying(100)  NOT NULL,
            thetime character varying(100)  NOT NULL,
            place character varying(1000)  NOT NULL,
            seatstableid character varying(1000),
            covid BOOLEAN NOT NULL DEFAULT false
         );';

        $result = $this->db->query($query);
         
        $html = file_get_contents($url);

        $start = stripos($html, '<h1 class="generic__title">');
        $end = stripos($html, '</h1>', $offset = $start);
        $length = $end - $start;
        $date = explode("-",substr($html, $start, $length))[1];
        $date = str_replace(" ","", $date);
        // echo $date;

         while(!Empty(stripos($html, 'class="match__entry__entry__top"'))){
            $arrayTeam = [];
            $start = stripos($html, 'class="match__entry__entry__top"');
            $end = stripos($html, 'class="match__entry__entry__bottom match__entry__entry__bottom--next"', $offset = $start + 50);
            $length = $end - $start;
            $htmlSection = substr($html, $start, $length);
            //echo $htmlSection;
            while((gettype(stripos($htmlSection, '<span class="title">')) == "integer")){
                $startTeam = stripos($htmlSection, '<span class="title">');
                $endTeam = stripos($htmlSection, '</span>', $offset = $startTeam);
                $lengthTeam = $endTeam - $startTeam;
                $htmlSectionTeam = substr($htmlSection, $startTeam, $lengthTeam);
                $arrayTeam[] =str_replace('<span class="title">',"", $htmlSectionTeam);;
                $htmlSection =  substr($htmlSection , $endTeam);
                // echo "\n$htmlSectionTeam";
            }

            $startTime = stripos($html, '<div class="date">');
            $endTime = stripos($html, '</div>', $offset = $startTime);
            $length = $endTime - $startTime;
            $time = substr($html, $startTime, $length);
            $time =str_replace('<div class="date">', "", $time);
            // echo $time;

            $price = rand(20, 100);
            $price = "$price euros";

            $startLocation = stripos($html, 'class="match__entry__entry__bottom match__entry__entry__bottom--next">');
            $endLocation = stripos($html, '</div>', $offset = $startLocation + 50);
            $length = $endLocation - $startLocation;
            $loc = substr($html, $startLocation, $length);
            $loc =str_replace('class="match__entry__entry__bottom match__entry__entry__bottom--next">', "", $loc);
            $loc =str_replace('<i class="fas fa-map-marker-alt"></i>', "", $loc);
            $loc = trim(preg_replace('/\s+/', ' ', $loc));
            // echo $loc;

            $html = substr($html,  $end);

            
            $arraymatch = [$arrayTeam[0], $arrayTeam[1], $date, $time, $loc];
            $row = $this->getIdofmatchOrCheckmatch($arraymatch);
            // echo count($row);
            

            if (count($row) == 0) {
                // echo 'data not exist';
                $query2 = "INSERT INTO frenchmatch(team1, team2, thedate, thetime, place) VALUES ('$arrayTeam[0]', '$arrayTeam[1]', '$date', '$time', '$loc');";
                $result = $this->db->query($query2);
                // echo "<br>";
                // $query16 = "SELECT id FROM frenchmatch WHERE (team1='$arrayTeam[0]'AND team2='$arrayTeam[1]' AND thedate='$date'AND thetime = '$time' AND place='$loc');";
                // $query16 = $this->db->query($query16);
                $results = $this->getIdofmatchOrCheckmatch($arraymatch);

                foreach ($results as $row) {
                    $seat_table_num_id =  $row["id"];
                    echo $seat_table_num_id;
                    $query12 = "UPDATE frenchmatch SET seatstableid = 'fr_match_seat_table_id_$seat_table_num_id' WHERE (team1='$arrayTeam[0]'AND team2='$arrayTeam[1]' AND thedate='$date'AND thetime = '$time' AND place='$loc');";
                    $result = $this->db->query($query12);

                    $queryRequest_create_seat_table = "CREATE TABLE IF NOT EXISTS fr_match_seat_table_id_$seat_table_num_id (
                        id character varying(100) PRIMARY KEY,
                        numid character varying(100) NOT NULL,
                        reservation character varying(100) NOT NULL, 
                        price character varying(100) NOT NULL,
                        ticketid character varying(100),
                        ticketreserver character varying(100),
                        ticketstate character varying(100)
                     );";
            
                    $do_queryRequest_create_seat_table = $this->db->query($queryRequest_create_seat_table);


                }      
                    
                }

            
            else{
                // echo 'data exist';
            }
         }
        
    return ($date);
    }
}