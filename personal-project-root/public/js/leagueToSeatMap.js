const btns = Array.from(document.getElementsByClassName('reservation_button'));

// program to generate range of numbers and characters
function* iterate(a, b) {
    for (let i = a; i <= b; i += 1) {
      yield i
    }
  }
  
  function range(a, b) {
      if(typeof a === 'string') {
          let result = [...iterate(a.charCodeAt(), b.charCodeAt())].map(n => String.fromCharCode(n));
          return(result);
      }
      else {
          let result = [...iterate(a, b)];
          return (result);
      }
  }

btns.forEach(btn => {
  btn.addEventListener(
    'click',  postJsFetch)});



function postJsFetch(event) {

  console.log('btn clicked', event.target.parentElement.childNodes);
  console.log(event.target.parentElement.childNodes[1].innerText);
  console.log(event.target.parentElement.childNodes[5].innerText);
  console.log(event.target.parentElement.childNodes[7].childNodes[1].childNodes[1].innerText);
  console.log(event.target.parentElement.childNodes[7].childNodes[3].childNodes[1].innerText);

  var place = event.target.parentElement.childNodes[7].childNodes[1].childNodes[1].innerText;
  var time =event.target.parentElement.childNodes[7].childNodes[3].childNodes[1].innerText;

    xhr = new XMLHttpRequest();
    xhr.open("GET","../../../../img/seats3.svg",false);
    // Following line is just to be on the safe side;
    // not needed if your server delivers SVG with correct MIME type
    xhr.overrideMimeType("image/svg+xml");
    xhr.onload = function(e) {
    // You might also want to check for xhr.readyState/xhr.status here

        var tag = xhr.responseXML.documentElement.getElementsByTagName("rect"); 

        var seat_client_id = range(1000, 1000 + tag.length);

        var seat_to_php = {};

        var seat_price_to_php = {};

        let min = 40;
        let max = 80;


        for(var i=0;i<tag.length;i++) {
        // console.log(tag[i].id);
        seat_to_php[tag[i].id] = seat_client_id[i];
        seat_price_to_php[tag[i].id] = Math.round(Math.random() * (max - min) + min);
        };

        // console.log(seat_to_php);

        let team1=  event.target.parentElement.childNodes[1].innerText;
        let team2= event.target.parentElement.childNodes[5].innerText;

        var data = {"seat": seat_to_php, 
        "price":seat_price_to_php,
        "team1":  team1,
            "team2":  team2,
            "date":document.querySelector("#date").innerText,
            "time":time, 
            "place":place,
        };

        fetch("http://localhost:8080/seatstoDB",{method:'post',body: JSON.stringify(data), credentials: 'same-origin',
        mode: 'same-origin',headers: {
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest"
        }}).then((response) => response.json())
        .then((res) => {
          console.log('Success:', "leagueToSeatsMap.js");
         console.log('Success:', res);
        window.location.replace("http://localhost:8080/seats/"+team1 + " vs " + team2 + "/"+res[0] + "/"+data["date"]+ "/"+data["time"] + "/"+data["place"]);
        });

        }
        xhr.send("");
}




