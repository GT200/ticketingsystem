var listChoosenSeat;
var listChoosenSeatPrice;
class Seats{
  seatsDBTableName;
  tagRectInSvg;

  constructor(DBtableID){
    this.seatsDBTableName = document.getElementById("tableID").innerText;
  }

  addSeatMapSvgToMap(svgImgPath){
    
    var xhr = new XMLHttpRequest();
    xhr.open("GET",svgImgPath,false);
    xhr.overrideMimeType("image/svg+xml");
    xhr.onload = function(e) {
      // // You might also want to check for xhr.readyState/xhr.status here
      document.getElementById("svgContainer").appendChild(xhr.responseXML.documentElement);
    };
    xhr.send("");
    this.tagRectInSvg = document.getElementsByTagName("rect"); 

  }

  fetchDataFromDataBase()
  {
    var data = {"tableId": this.seatsDBTableName};
    fetch("http://localhost:8080/seatsDBtoView/",
          {method:'post',
          body: JSON.stringify(data), 
          credentials: 'same-origin',
          mode: 'same-origin',
          headers: {"Content-Type": "application/json",
                    "X-Requested-With": "XMLHttpRequest"
                    }
          }).then((response) => response.json())
            .then((json_seats_data) => 
            {
              console.log('Success:', json_seats_data);
              Object.entries(json_seats_data).forEach(([key, value]) => 
              {
                // console.log(key, value.reservation);
                for(var i=0;i<this.tagRectInSvg.length;i++) 
                {
                  // console.log(value.id);
                  if (this.tagRectInSvg[i].id == value.id)
                  {
                    this.tagRectInSvg[i].setAttribute("class", "Seat: " + value.numid+"<br>Price: " +value.price + "$<br>Reservation: " +value.reservation);
                    if (value.reservation =="reserved"){this.tagRectInSvg[i].style.fill="green"; }
                    if (value.reservation =="COVID"){this.tagRectInSvg[i].style.fill="black"; }
                  }
                }
              });
            });
  }

  
  addEventOnSeatClick(){
    document.getElementById("submitChooseSeat").addEventListener("click", this.sendChooseSeatToDB)
    var map = L.map('map').setView([51.505, -0.09], 13);
    listChoosenSeat = {};
    listChoosenSeatPrice={};

    var icon = L.divIcon({
      className: 'seat',
      html: document.getElementById("svgContainer").innerHTML,
      iconSize: [300, 100],
      iconAnchor: [550, 550]
      });
      var markerMap = L.marker([51.5, -0.09], { icon: icon }).addTo(map);

    for(var i=0;i<this.tagRectInSvg.length;i++)
    {     
      this.tagRectInSvg[i].addEventListener('click',  function(e) 
        {
          console.log(e.target.id);
          console.log(e.target.style.fill);
          if (e.target.style.fill=="rgb(172, 157, 147)" || e.target.style.fill=="rgb(200, 190, 183)")
          {
                console.log(e.target.style.fill);
                e.target.style.fill="red";
                var li = document.createElement("li");
                var seatPrice = e.target.className.baseVal.split("<br>")[1].split(':')[1].replace("$", "");
                var seatNum = e.target.className.baseVal.split("<br>")[0].split(':')[1].replace(" ", "");
                li.setAttribute('id', toString(seatNum));
                li.setAttribute('class', "list-group-item");
          
                li.innerText = "seat n° " + seatNum +":  "  +seatPrice;
                listChoosenSeat[eval(seatNum)] = li;
                listChoosenSeatPrice[seatNum+"_price"] = eval(seatPrice);
          }
          else if (e.target.style.fill=="red")
          {
            console.log(listChoosenSeat);
            console.log(e.target.className.baseVal);
            var seatNum = e.target.className.baseVal.split("<br>")[0].split(':')[1];
            e.target.style.fill="rgb(172, 157, 147)"; 
            console.log(e.target.style.fill);
            delete listChoosenSeat[eval(seatNum)];
            console.log(listChoosenSeat);
            document.getElementById("choosenSeats").innerHTML = "";
          }
          var sumPrice = 0;
          Object.keys(listChoosenSeat).forEach(function (key) {
            document.getElementById("choosenSeats").appendChild(listChoosenSeat[key]);
            sumPrice +=(listChoosenSeatPrice[key+"_price"]);
            
         });
         document.getElementById("sumPrice").innerText="Total Price: " + sumPrice;
         document.getElementById("payTotalPrice").innerText="Pay: " + sumPrice;
          // listChoosenSeat.forEach((seatInfo) =>{
          //   console.log(seatInfo)
          //   document.getElementById("totalPrices").appendChild(seatInfo);
          // })
          


        });

        var tooltip = document.getElementById("tooltip");
        this.tagRectInSvg[i].addEventListener("mouseover", function( event ) {
          // alert("mouse over test!")}, false);
          // console.log(event.target.className.baseVal);
          // console.log(event.clientX);

          // markerMap.getPopup().setContent(event.target.className.baseVal)

          
          tooltip.innerHTML=event.target.className.baseVal;
          tooltip.style.visibility="visible";
          tooltip.style.top= (event.clientY -100 )+"px";
          tooltip.style.left = (event.clientX )+"px";

        });

        this.tagRectInSvg[i].addEventListener("mouseout", function( event ) {
          // alert("mouse over test!")}, false);
          // console.log(event.target.className.baseVal);
          // markerMap.getPopup().setContent("choose a seat")

          // // tooltip.innerText=event.target.className.baseVal;
          tooltip.style.visibility="hidden";
          tooltip.style.top= (event.clientY - 200)+"px";
          tooltip.style.left = (event.clientX - 50)+"px";
        });



    }
  }

  sendChooseSeatToDB(){
    console.log(listChoosenSeat);
    var data = {"choosenSeatsId": JSON.stringify(listChoosenSeat), "tableId": document.getElementById("tableID").innerText, 
                "ReserverName": document.getElementById("inputReserverName").value};
    fetch("http://localhost:8080/jsChoosenSeatToDB/",
          {method:'post',
          body: JSON.stringify(data), 
          credentials: 'same-origin',
          mode: 'same-origin',
          headers: {"Content-Type": "application/json",
                    "X-Requested-With": "XMLHttpRequest"
                    }
          }).then((response) => response.json())
            .then((json_seats_data) => 
            {

              console.log('Success:', json_seats_data);
              document.getElementById("transToTickt").innerHTML=json_seats_data;
              document.getElementById("paymentCardFooter").innerHTML= '<a href="" class="btn btn-primary mr-auto">Choose new seats</a>';

              document.getElementById("paymentCardFooter").appendChild(document.getElementById("printBtn"));
              document.getElementById("printBtn").style.display="block";

            }
            );
  }

}
