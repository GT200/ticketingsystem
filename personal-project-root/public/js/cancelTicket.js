
class CancelTicket{
    cancelBtn;

    constructor(){
        this.cancelBtn = document.getElementById("cancelReservation");
        this.cancelBtn.addEventListener("click", this.confirmCancel);
    }

    confirmCancel(){
        var ticketID = document.getElementById("cancledTicketId").value;
        var cancelTicketReserverName = document.getElementById("cancelTicketReserverName").value;
        var cancelReserverEmail = document.getElementById("cancelReserverEmail").value;
        var cancledVisaAccountName = document.getElementById("cancledVisaAccountName").value;
        var cancelIban = document.getElementById("cancelIban").value;

        console.log(ticketID);
        var data = {"ticketID": ticketID,"cancelTicketReserverName": cancelTicketReserverName, "cancelReserverEmail": cancelReserverEmail, "cancledVisaAccountName": cancledVisaAccountName,  "cancelIban":cancelIban};
        fetch("http://localhost:8080/cancelTicket",{method:'post',body: JSON.stringify(data), credentials: 'same-origin',
        mode: 'same-origin',headers: {
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest"
        }}).then((response) => response.json())
        .then((json) => {
          console.log('Success:', "cancelTicket.js");
          console.log('Success:', json);
          document.getElementById("cancledTicketId").value = json;

        });

    }

}

var objCancTick = new CancelTicket();