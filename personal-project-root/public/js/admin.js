let myCovidSwitches = document.querySelectorAll(".covidSwitch");

myCovidSwitches.forEach(covidswitch => {
  covidswitch.addEventListener('click', 
  function() {
    // alert(covidswitch.id);
    if (document.getElementById("inlineRadio1").checked && covidswitch.checked ){
      var idMatch = covidswitch.id.split("-")[1];
      var tb_name = "fr_match_seat_table_id_"+ idMatch;
      var data = {"tb_name": tb_name , "league":"frenchmatch", "idMatch":idMatch};
        fetch("http://localhost:8080/setCovid",
        {method:'post',
        body: JSON.stringify(data), 
        credentials: 'same-origin',
        mode: 'same-origin',
        headers: {
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest"
        }}).then((response) => response.json())
        .then((json) => {
          console.log('Success:', "admin.js.js");
          console.log('Success:', json);
        });

    }

    else if (document.getElementById("inlineRadio1").checked && !covidswitch.checked ){
      var idMatch = covidswitch.id.split("-")[1];
      var tb_name = "fr_match_seat_table_id_"+ idMatch;
      var data = {"tb_name": tb_name , "league":"frenchmatch", "idMatch":idMatch};
        fetch("http://localhost:8080/unSetCovid",
        {method:'post',
        body: JSON.stringify(data), 
        credentials: 'same-origin',
        mode: 'same-origin',
        headers: {
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest"
        }}).then((response) => response.json())
        .then((json) => {
          console.log('Success:', "admin.js.js");
          console.log('Success:', json);
        });

    }
})
});