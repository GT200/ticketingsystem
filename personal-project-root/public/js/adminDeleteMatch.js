const delBtn = Array.from(document.getElementsByClassName('deleteBtn'));

// program to generate range of numbers and characters
function* iterate(a, b) {
    for (let i = a; i <= b; i += 1) {
      yield i
    }
  }
  
  function range(a, b) {
      if(typeof a === 'string') {
          let result = [...iterate(a.charCodeAt(), b.charCodeAt())].map(n => String.fromCharCode(n));
          return(result);
      }
      else {
          let result = [...iterate(a, b)];
          return (result);
      }
  }

delBtn.forEach(btn => {
  btn.addEventListener(
    'click',  postJsFetch)});



function postJsFetch(event) {

  var idMatch = event.target.value;
  
  var data = {"idMatch":idMatch, 
  "tb_name":"frenchmatch"
  };

  console.log("delete")

        fetch("http://localhost:8080/adminDelete",{method:'post',body: JSON.stringify(data), credentials: 'same-origin',mode: 'same-origin',headers: {"Content-Type": "application/json","X-Requested-With": "XMLHttpRequest"}
      })

        
}




