# TicketingSystem

# APPLICATION DEVELOPPER SOUS CODEIGNITER, UN FRAMEWORK PHP

## DOCUMENTION DEV
### Les dossiers importants (attention: le framework implémente le modèle MVP)

- ./personal-project-root/app/view => implémente les pages présenter à l'utilisateur 
  - Template => le header et footer standard du site 
  - HomePage => page d'accueil
  - Pages => les principaux pages
- ./personal-project-root/app/controllers => implémente l'intéraction entre l'interface et la base de données
- ./personal-project-root/app/Models => implémte la récupération des données et la mise à jours des données.

- ./personal-project-root/app/Config:  
  - Database => configuration de la base de données

## VIDEO DEMO
- disponible à la racine du projet 
## DEMARRER LE SERVER 
- au niveau du dossier ./personal-project-root/ , lancer: 
  - php spark serve

## Demmarrer sur docker: 
- telecharger composer sur votre machine et lancer au niveau du dossier ./personal-project-root/
  - composer install
  - un docker compose file est disponible à la racine (mais tester que sur windows) => lancer au niveau de la racine du projet docker compose up 


